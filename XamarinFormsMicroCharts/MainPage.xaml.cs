﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamarinFormsMicroCharts
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            CreateChartEntries();

        }

        private void CreateChartEntries()
        {
            var entries = new List<Microcharts.Entry>();
            var entrySet1 = new Microcharts.Entry(200)
            {
                Label = "A",
                ValueLabel = "A"
            };
            var entrySet2 = new Microcharts.Entry(200)
            {
                Label = "B",
                ValueLabel = "B"
            };

            entries.Add(entrySet1);
            entries.Add(entrySet2);

            var chart = new BarChart() { Entries = entries };
            chartInView.Chart = chart;
        }
    }
}
